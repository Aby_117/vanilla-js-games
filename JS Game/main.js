var userScore = 0;
var compScore = 0;
const userScore_span = document.getElementById("user-score");
const compScore_span = document.getElementById("computer-score");
const scoreBoard_div = document.querySelector(".score-board");
const result_p = document.querySelector(".result p");
const rock_div = document.getElementById("rock");
const paper_div = document.getElementById("paper");
const sci_div = document.getElementById("sci");
const computer_active = document.getElementById("comp-base");


function getComputerChoice() {
	computer_active.style.display = "block";
	const choices = ['r', 'p', 's'];
	const randomNumber = Math.floor(Math.random() * 3);
	return choices[randomNumber];
}

//add on 
function outPut(cChoice) {
	console.log(cChoice);
	if (cChoice == "r"){
		document.getElementById("cimage").innerHTML ="<img src='https://img.icons8.com/color/2x/hand-rock.png'>";
	}
	else if (cChoice == "p"){
		document.getElementById("cimage").innerHTML ="<img src='https://img.icons8.com/color/2x/hand.png'>";
	}
	else {document.getElementById("cimage").innerHTML = "<img src='https://img.icons8.com/color/2x/hand-scissors.png'>";}
}

function valueOfWord(user) {
	if (user == 'r') {
		val = 'rock';
		return val;
	}
	else if (user == 'p') {
		val = 'paper';
		return val;
	}
	else { 
		val = "sci";
		return val;
	}
}


function convertToWord(letter) {
	if (letter == "r") return "Rock";
	if (letter == "p") return "Paper";
	return "Scissor";  
}

function win(user, computer) {
	var val = '';
	userScore++;
	userScore_span.innerHTML = userScore;
	const smallUserWord = "user".fontsize(3).sup();
	const smallCompWord = "comp".fontsize(3).sup();
	val = valueOfWord(user);
	document.getElementById(val).classList.add('green-glow');
	setTimeout(function(){document.getElementById(val).classList.remove('green-glow')}, 500);
	result_p.innerHTML = convertToWord(user) + smallUserWord + " beats " + convertToWord(computer) + smallCompWord + ". You Win!";
}

function lose(user, computer) {
	compScore++;
	compScore_span.innerHTML = compScore;
	const smallUserWord = "user".fontsize(3).sup();
	const smallCompWord = "comp".fontsize(3).sup();
	val = valueOfWord(user);
	document.getElementById(val).classList.add('red-glow');
	setTimeout(function(){document.getElementById(val).classList.remove('red-glow')}, 500);
	//different way to write variables and strings in JS.
	result_p.innerHTML = `${convertToWord(computer)}${smallCompWord} beats ${convertToWord(user)}${smallUserWord}. You Lose!`;
}

function draw(user, computer) {
	val = valueOfWord(user);
	document.getElementById(val).classList.add('grey-glow');
	//Can use '=>' instead of curly braces if it's a one-liner.
	setTimeout(() => document.getElementById(val).classList.remove('grey-glow'), 500);
	result_p.innerHTML = "Draw.";
}

function game(userChoice) {
	const computerChoice = getComputerChoice();

	//addon 
	outPut(computerChoice);

	switch (userChoice + computerChoice) {
		case "rs":
		case "pr":
		case "sp":
			win(userChoice, computerChoice);
			break;
		case "rp":
		case "sr":
		case "ps":
			lose(userChoice, computerChoice);
			break;
		case "rr":
		case "pp":
		case "ss":
			draw(userChoice, computerChoice);
			break;
	}
}



function main() {
	rock_div.addEventListener('click', function() {
		game('r');
	})

	paper_div.addEventListener('click', function() {
		game('p');
	})

	sci_div.addEventListener('click', function() {
		game('s');
	})
}

main();