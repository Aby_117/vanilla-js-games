# vanilla-js-games

## Description
Games I had made when I was learning vanilla JavaScript.

## Installation
Just dowload the repository and go into a game's project directory. There you can click on `index.html` to run. Other `.html` files may also contain mini-games.

## Roadmap
Most of these projects are simplistic projects that had helped me grasp basic JavaScript.
##### 1. Rock, Paper, Scissors
A simple game where you play rock, paper, scissors against the computer. Keeps track of what you choose and what the computer chooses, scoring accordingly.

##### 2. Reaction Tester
A simple game that determines your click reaction time depending on how fast you click the randomly popping circles and squares.


## Contributing
Since these are my personal projects, there have been no other contributors to them.

## Authors and acknowledgment
Aby

## License
No Public License

## Project status
Complete.
